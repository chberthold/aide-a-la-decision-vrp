// tp_vrp.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//
#include "utils.h"
#include <iostream>

int main()
{
	//srand(time(NULL));
	srand(0);
    T_inst inst;
	T_sol s;
	int client;
    lire_fichier("P:/Aide_dec/tp_vrp/paris.txt", inst);

	std::cout << inst.n << endl;
	std::cout << inst.capacite << endl; 

    /* For each client */
		for (client = 0; client <= inst.n; client++) {

			/* For each voisin */
			for (int voisin = 0; voisin <= inst.n; voisin++) {
				std::cout << "Distance " << client << " et " << voisin << " = " << inst.dist[client][voisin] << endl;
			}
		}

	for (client = 1; client <= inst.n; client++) {
		std::cout << "Demande du client " << client << " : " << inst.demande[client] << endl;
	}

	//generer_tour_geant(inst, s);
	plus_proche_voisin(inst, s);
	printf("Tour geant non random  : ");
	for (client = 0; client <= inst.n + 1; client++) {
		std::cout << s.tour_geant[client] << " ";
	}
	std::cout << endl;

	generer_tour_geant(inst, s);
	printf("Tour geant random  : ");
	for (client = 0; client <= inst.n + 1; client++) {
		std::cout << s.tour_geant[client] << " ";
	}
	std::cout << endl;

	autre_heuristique(inst, s);
	printf("Tour geant autre  : ");
	for (client = 0; client <= inst.n + 1; client++) {
		std::cout << s.tour_geant[client] << " ";
	}
	std::cout << endl;

	evaluer(inst, s);
	for (client = 0; client <= inst.n; client++) {
		std::cout << "client " << client << " : " << s.m[client] << endl;
	}
	
	for (client = 0; client <= inst.n; client++) {
		std::cout << "client " << client << " pere : " << s.pere[client] << endl;
	}

	for (int tournee = 1; tournee <= s.nb_tournees; tournee++) {
		std::cout << "Tournee " << tournee << " cout : " << s.liste_t[tournee].cout << endl;

		for (client = 1; client <= s.liste_t[tournee].nb; client++) {
			std::cout << "Client : " << s.liste_t[tournee].liste[client] << endl;
		}
	}
	
	recherche_locale(inst, s, 100);
	std::cout << "end : " << s.cout << endl;

	grasp(s, inst, 100, 10, 3);
	std::cout << "end : " << s.cout << endl;
	
	return 0; 
}
