#pragma once
using namespace std;
#include <iostream>
#include <fstream>
#include <string>

const int K = 100000;
const int nmax_clients = 200;
const int nmax_clients_par_tournee = 50;
const int nmax_tournee = 50;
const int c_inf = 999999;


typedef struct T_inst {
	int n;                                     //Nombre de villes de l'instance
	int vehicule;                              //Nombre de vehicules
	int capacite;                              //Capacite de la flotte de vehicules
	int dist[nmax_clients][nmax_clients];      //Tableau des distances entre villes
	int demande[nmax_clients];                 //Tableau des demandes clients
}T_inst;

typedef struct T_trip{
	int nb;                                    //Nombre de clients desservis dans la tournee
	int cout;                                  //Cout total de la tournee
	int liste[nmax_clients_par_tournee];       //Liste des clients desservis dans l'ordre
	int demande[nmax_clients_par_tournee];     //Liste cumulative de la demande stockee pendant la tournee a l'etape i
}T_trip;

typedef struct T_sol {
	int nb_tournees;                           //Nombre de tournees effectuees dans la solutions
	T_trip liste_t[nmax_tournee];              //Liste des tournees effectuees
	int cout;                                  //Cout total du tour geant
	int m[nmax_clients];                       //Date d'arrivee a chaque sommets
	int pere[nmax_clients];                    //Sommet pere pour chaque sommet
	int tour_geant[nmax_clients];              //Tour geant
}T_sol;


typedef struct T_min {
	int min[6][3];                              //Distances minimales (4 voisins) [0] = client, [1] = distance, [2] = ou il est dans l
	int l[nmax_clients];                        //Clients encore a traiter
	int taille_l;                               //Taille du tableau l effective
	int nb_voisins;                             //Nombre de voisins effectif
	int i;                                      //Sommet dont on cherche les distances min
}T_min;


void lire_fichier(string nom, T_inst & inst);
void generer_tour_geant(T_inst& inst, T_sol& s);
void plus_proche_voisin(T_inst& inst, T_sol& s);
void autre_heuristique(T_inst& inst, T_sol& s);
void recalcul_demandes(T_inst& inst, T_trip& t);
void recherche_locale(T_inst& inst, T_sol& sol, int nb_iter);
void evaluer(T_inst& inst, T_sol& s);
void grasp(T_sol& s, T_inst& inst, int nb_iter, int nb_els, int nb_voisins);