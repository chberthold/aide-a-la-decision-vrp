#include "utils.h"

int Ti[K] = { 0 };

/** -------------------------------------------------- *
  * @fn lier_ficher                                    *
  *                                                    *
  * @brief Permet de lire le ficher d'infos            *
  *                                                    *
  * @param nom {string} L'emplacement de fichier       *
  * @param inst {T_inst} Structure a stocker           *
  *--------------------------------------------------- */
void lire_fichier(string nom, T_inst& inst) {
	int      client;
	ifstream infile(nom);

	/* Si le fichier n'a pas pu etre ouvert */
	if (!infile.is_open())
	{
		cout << "can not open the file \n" << endl;
		exit(-1);
	}
	else {
		infile >> inst.n;
		infile >> inst.vehicule;
		infile >> inst.capacite;

		/* For each client */
		for (client = 0; client <= inst.n; client++) {

			/* For each voisin */
			for (int voisin = client; voisin <= inst.n; voisin++) {
				infile >> inst.dist[client][voisin];
				inst.dist[voisin][client] = inst.dist[client][voisin];
			}
			inst.dist[inst.n + 1][client] = -1;
			inst.dist[client][inst.n + 1] = -1;
		}

		/* Remplissage de la demande des clients */
		for (client = 1; client <= inst.n; client++) {
			infile >> inst.demande[client];
		}

		inst.dist[inst.n + 1][0] = -1;

	}

	infile.close();
}

/** --------------------------------------------------------- *
  * @fn pemuter_min                                           *
  *                                                           *
  * @brief Permute les elements dans le tableau dans le cadre *
  *			de la recherche des plus proches voisins          *
  *                                                           *
  * @param min   {T_min} Structure où stocker les voisins     *
  * @param where {int}   Endroit d'ou commence la permutation *
  * --------------------------------------------------------- */
void permuter_min(T_min& min, int where) {
	int save[3];

	/* Tant que l'element n'est pas a la bonne place */
	while (min.min[where][1] < min.min[where - 1][1]) {
		save[0] = min.min[where][0];
		save[1] = min.min[where][1];
		save[2] = min.min[where][2];

		min.min[where][0] = min.min[where - 1][0];
		min.min[where][1] = min.min[where - 1][1];
		min.min[where][2] = min.min[where - 1][2];

		min.min[where - 1][0] = save[0];
		min.min[where - 1][1] = save[1];
		min.min[where - 1][2] = save[2];

		where--;
	}
}

/** -------------------------------------------------------- *
  * @fn generer_min                                          *
  *                                                          *
  * @brief Recupere les plus proches voisins du sommet min.i *
  *                                                          *
  * @param inst {T_inst} Structure contenant le graphe du VRP*
  * @param min  {T_min}  Structure ou stocker les plus       *
  *                      proches voisins                     *
  * -------------------------------------------------------- */
void generer_min(T_inst& inst, T_min& min) {
	int empile = 0;
	min.nb_voisins = 0;

	/* Pour tous les clients qu'il reste a traiter */
	for (int i = 0; i <= min.taille_l; i++) {
		min.min[empile][0] = min.l[i];
		min.min[empile][1] = inst.dist[min.i][min.l[i]];
		min.min[empile][2] = i;

		permuter_min(min, empile);
		
		/* Si on a moins de quatre voisins (sans compter les cases 0
		   et 5 qui sont des extremes (-infini, +infini)*/
		if (empile < 5) {
			min.nb_voisins++;
			empile++;
		}
	}

	min.nb_voisins--;
}

/** -------------------------------------------------------------------- *
  * @fn generer_tour_geant                                               *
  *                                                                      *
  * @brief Creation du tour geant avec le plus proche voisin randomise   *
  *                                                                      *
  * @param inst {T_inst} Structure contenant le graphe du VRP            *
  * @param s    {T_sol}  Structure ou stocker le tour geant              *   
  * -------------------------------------------------------------------- */
void generer_tour_geant(T_inst& inst, T_sol& s) {
	T_min min;
	int   i;
	int   cont;
	int   r;
	int   which;

	/* Initialisation des clients a traiter */
	for (i = 1; i <= inst.n; i++) {
		min.l[i] = i;
	}
	min.l[0] = inst.n + 1;

	min.taille_l = inst.n;
	s.tour_geant[0] = 0;
	min.i = 0;

	//Pour tous les clients
	for (i = 1; i <= inst.n; i++) {
		generer_min(inst, min);

		which = 1;
		cont = 1;

		//Tant qu'on a pas trouve quel voisin on voulait prendre en 'random'
		while (cont) {
			r = rand() % 101;

			//Si on decide de prendre le voisin 'which'
			if (r <= 80) {
 				s.tour_geant[i] = min.min[which][0];
				min.l[min.min[which][2]] = min.l[min.taille_l];
				min.taille_l--;
				cont = 0;
			}

			else {
				which++;

				//Si on a atteint le dernier voisin dans la liste, on le choisit automatiquement
				if (which >= min.nb_voisins) {
					s.tour_geant[i] = min.min[which][0];
					min.l[min.min[which][2]] = min.l[min.taille_l];
					min.taille_l--;
					cont = 0;
				}
			}
		}

		min.i = s.tour_geant[i];
	}

	s.tour_geant[inst.n + 1] = 0;
}

/** ------------------------------------------------------- *
  * @fn which_min                                           *
  *                                                         *
  * @brief Determination du plus proche voisin d'un sommet  *
  *                                                         *
  * @param inst {T_inst} Structure stockant les infos du VRP*
  * @param min  {T_min}  Structure stockant les clients     *
  *                      qu'il faut encore traiter          *
  * ------------------------------------------------------- */
int which_min(T_inst& inst, T_min& min) {
	int mini   = inst.dist[min.i][min.l[1]];
	int save_i = 1;

	// Pour tous les clients qui restent
	for (int i = 2; i <= min.taille_l; i++) {

		// Si le client est plus proche que le dernier enregistre
		if (mini > inst.dist[min.i][min.l[i]]) {
			mini = inst.dist[min.i][min.l[i]];
			save_i = i;
		}
	}

	mini = min.l[save_i];
	min.l[save_i] = min.l[min.taille_l];
	min.taille_l--;

	return mini;
}


/** --------------------------------------------------------- *
  * @fn plus_proche_voisin                                    *
  *                                                           *
  * @brief Creation du tour geant avec le plus proche voisin  *
  *        non randomisee                                     *
  *                                                           *
  * @param inst {T_inst} Structure contenant les infos du VRP *
  * @param s    {T_sol}  Structure ou stocker le tour geant   *
  * --------------------------------------------------------- */
void plus_proche_voisin(T_inst& inst, T_sol& s) {
	T_min min;
	int   i;
	int   which;

	// Init des clients a traiter
	for (i = 1; i <= inst.n; i++) {
		min.l[i] = i;
	}

	min.taille_l = inst.n;
	s.tour_geant[0] = 0;
	min.i = 0;

	//Pour tous les clients
	for (i = 1; i <= inst.n; i++) {
		s.tour_geant[i] = which_min(inst, min);
		min.i = s.tour_geant[i];
	}

	s.tour_geant[inst.n + 1] = 0;

}

/** ------------------------------------------------------------------ *
  * @fn autre_heuristique                                              *
  *                                                                    *
  * @brief Creation d'un tour geant en allant au plus loin du depot    *
  *        tant que la moitie de la demande totale n'a pas ete traitee *
  *        puis en revenant au plus proche du depot (parmis les 4 plus *
  *        proches voisins)                                            *
  *                                                                    *
  * @param inst {T_inst} Structure contenant les infos du VRP          *
  * @param s    {T_sol}  Structure ou stocker le tour geant            *
  * ------------------------------------------------------------------ */
void autre_heuristique(T_inst& inst, T_sol& s) {
	T_min min;
	int   i;
	int   j;
	int   which;
	int   capa_cur   = 0;
	int   capa_total = 0;
	int   dist_depot = 0;

	// Init des clients a traiter
	for (i = 1; i <= inst.n; i++) {
		min.l[i] = i;
		capa_total += inst.demande[i];
	}
	min.l[0] = inst.n + 1;

	min.taille_l = inst.n;
	s.tour_geant[0] = 0;
	min.i = 0;

	//Pour tous les clients
	for (i = 1; i <= inst.n; i++) {
		generer_min(inst, min);

		/* Si on a traite moins de la moitie de la demande, on prend
			le voisin le plus loin du depot                          */
		if (capa_cur < capa_total / 2) {
			dist_depot = 0;

			/* Recherche du plus loin du depot */
			for (j = 1; j <= min.nb_voisins; j++) {

				if (inst.dist[0][min.min[j][0]] > dist_depot) {
					which = j;
					dist_depot = inst.dist[0][min.min[j][0]];
				}

			}

		}

		/* Sinon, on prend le plus proche */
		else {
			dist_depot = c_inf;

			/* Recherche du plus proche du depot*/
			for (j = 1; j <= min.nb_voisins; j++) {

				if (inst.dist[0][min.min[j][0]] < dist_depot) {
					which = j;
					dist_depot = inst.dist[0][min.min[j][0]];
				}

			}

		}

		s.tour_geant[i] = min.min[which][0];
		min.l[min.min[which][2]] = min.l[min.taille_l];
		min.taille_l--;
		min.i = s.tour_geant[i];
		capa_cur += inst.demande[s.tour_geant[i]];
	}

	s.tour_geant[inst.n + 1] = 0;

}


/** ------------------------------------------------------------------ *
  * @fn determiner_tournees                                            *
  *                                                                    *
  * @brief Permet de remplir les tournees avec une evaluation donnee   *
  *                                                                    *
  * @param inst {T_inst} Structure contenant les infos du VRP          *
  * @param s    {T_sol}  Structure ou stocker les tournees             *
  * ------------------------------------------------------------------ */
void determiner_tournees(T_inst& inst, T_sol& s) {
	int tournee   = 0;
	int save_pere = -2;
	int i;
	int t;
	int max       = 0;
	int p_t;

	/* Pour tous les clients, on determine a quelle tournee ils appartiennent */
	for (i = 1; i <= inst.n; i++) {
		t = s.tour_geant[i];
		p_t = s.pere[t];

		/* Si le client appartient a une tournee differente de la derniere */
		if (p_t != save_pere) {

			/* Si ce n'est pas le debut */
			if (tournee != 0) {
				s.liste_t[tournee].liste[s.liste_t[tournee].nb + 1] = 0;
				s.liste_t[tournee].cout = s.m[s.tour_geant[i - 1]] - s.m[save_pere];

				/* Enregistrement du cout max */
				if (s.liste_t[tournee].cout > max) {
					max = s.liste_t[tournee].cout;
				}
			}
			tournee++;
			s.liste_t[tournee].demande[0] = 0;
			s.liste_t[tournee].nb = 0;
			s.liste_t[tournee].liste[0] = 0;
			save_pere = p_t;
		}

		s.liste_t[tournee].nb++;
		s.liste_t[tournee].liste[s.liste_t[tournee].nb] = t;
		s.liste_t[tournee].demande[s.liste_t[tournee].nb] = s.liste_t[tournee].demande[s.liste_t[tournee].nb - 1] + inst.demande[t];
	}
	s.liste_t[tournee].cout = s.m[s.tour_geant[inst.n]] - s.m[s.pere[s.tour_geant[inst.n]]];

	/* Enregistrement du cout max */
	if (s.liste_t[tournee].cout > max) {
		max = s.liste_t[tournee].cout;
	}

	s.liste_t[tournee].liste[s.liste_t[tournee].nb + 1] = 0;
	s.nb_tournees = tournee;
	s.cout = max;
}

/** --------------------------------------------------------- *
  * @fn evaluer                                               *
  *                                                           *
  * @brief Permet de remplir la structure solution            *
  *                                                           *
  * @param inst {T_inst} Structure contenant les infos du VRP *
  * @param s    {T_sol}  Structure ou stocker la solution     *
  * --------------------------------------------------------- */
void evaluer(T_inst& inst, T_sol& s) {
	int cout;
	int vol;
	int j;
	int tg;
	int tg_minus;
	
	s.cout = 0;

	/* Initialisation de la solution */
	for (int i = 0; i <= inst.n; i++) {
		s.m[i] = c_inf;
		s.pere[i] = -1;
	}

	s.m[0] = 0;

	/* Pour tous les clients */
	for (int i = 0; i < inst.n; i++) {
		j = i + 1;
		vol = 0;

		/* Tant que l'on a pas traite tous les autres clients et qu'on ne depasse pas la capacite */
		while ((j < inst.n + 1) && ((vol + inst.demande[s.tour_geant[j]]) < inst.capacite)) {

			tg = s.tour_geant[j];
			tg_minus = s.tour_geant[j - 1];
			
			/* Si on ne traite qu'un seul client dans la tournee */
			if (j == i + 1) {
				cout = inst.dist[0][tg] + inst.dist[tg][0];
				vol = inst.demande[tg];

			}

			else{
				vol += inst.demande[tg];
				cout += inst.dist[tg_minus][tg] + inst.dist[tg][0] - inst.dist[tg_minus][0];
			}

			/* Si la date d'arrivee calculee est plus tot que celle enregistree */
			if (s.m[s.tour_geant[i]] + cout < s.m[tg]) {
				s.m[tg] = s.m[s.tour_geant[i]] + cout;
				s.pere[tg] = s.tour_geant[i];
			}

			j++;
		}
		
	}

	determiner_tournees(inst, s);

}

/** --------------------------------------------------------- *
  * @fn recalcul_demandes                                     *
  *                                                           *
  * @brief Permet de calculer les demandes pendant une tournee*
  *                                                           *
  * @param inst {T_inst} Structure contenant les infos du VRP *
  * @param t    {T_trip} Structure ou stocker les demandes    *
  * --------------------------------------------------------- */
void recalcul_demandes(T_inst& inst, T_trip& t) {

	/* Pour tous les clients de la tournee */
	for (int i = 1; i <= t.nb; i++) {
		t.demande[i] = t.demande[i - 1] + inst.demande[t.liste[i]];
	}
}


/** --------------------------------------------------------- *
  * @fn permut_detour                                         *
  *                                                           *
  * @brief Effectue des permutations pour placer un element   *
  *        a la bonne place dans un tableau trie              *
  *                                                           *
  * @param detour {int [][]} Tableau ou faire les permutations*
  * @param where  {int}      Indice de l'element a deplacer   *
  * --------------------------------------------------------- */
void permute_detour(int detour[nmax_clients_par_tournee][3], int where) {
	int save[3];

	/* Tant que l'element n'est pas a la bonne place */
	while (where > 1 && detour[where][1] > detour[where - 1][1]) {
		save[0] = detour[where][0];
		save[1] = detour[where][1];
		save[2] = detour[where][2];

		detour[where][0] = detour[where - 1][0];
		detour[where][1] = detour[where - 1][1];
		detour[where][2] = detour[where - 1][2];

		detour[where - 1][0] = save[0];
		detour[where - 1][1] = save[1];
		detour[where - 1][2] = save[2];

		where--;
	}
}


/** --------------------------------------------------------- *
  * @fn remplir_detour                                        *
  *                                                           *
  * @brief Calcule les detours que representent chaque sommet *
  *        et les renseigne dans un tableau                   *
  *                                                           *
  * @param detour {int [][]} Tableau a remplir                *
  * @param inst   {T_inst}   Structure contenant les infos du *
  *                          VRP                              *
  * @param t      {T_trip}   Tournee consideree               *
  * --------------------------------------------------------- */
void remplir_detour(T_inst& inst, T_trip& t, int detour[nmax_clients_par_tournee][3]) {

	/* Pour tous les clients de la tournee */
	for (int i = 1; i <= t.nb; i++) {
		detour[i][0] = t.liste[i];
		detour[i][1] = inst.dist[t.liste[i - 1]][t.liste[i]] + inst.dist[t.liste[i + 1]][t.liste[i]];
		detour[i][2] = i;
		permute_detour(detour, i);
	}
}

/** --------------------------------------------------------- *
  * @fn decale_one                                            *
  *                                                           *
  * @brief Decale les element du tableau donne en parametre   *
  *                                                           *
  * @param t1     {T_trip}   tournee consideree               *
  * @param detour {int [][]} tableau dans lequel decaler      *
  * @param i      {int}      element dans le tableau a partir *
  *                          duquel decaler                   *
  * @param j      {int}      indice de fin du decalage        *
  * --------------------------------------------------------- */
void decale_one(T_trip t1, int detour[nmax_clients_par_tournee][3], int i, int j) {
	int a;

	/* Si l'element est moins loin que j */
	if (detour[i][2] < j) {

		/* Decalage a gauche */
		for (a = detour[i][2]; a < j - 1; a++) {
			t1.liste[a] = t1.liste[a + 1];
		}
	}

	else {

		/* Decalage a droite */
		for (a = detour[i][2]; a > j; a--) {
			t1.liste[a] = t1.liste[a - 1];
		}
	}
	
	t1.liste[a] = detour[i][0];
}

/** --------------------------------------------------------- *
  * @fn re_insert_simple                                      *
  *                                                           *
  * @brief Effectue le deplacement d'un client dans la tournee*
  *        consideree en reduisant le detour qu'il represente *
  *                                                           *
  * @param inst {T_inst} structure contenant les infos du VRP *
  * @param t    {T_trip} tournee consideree                   *
  * --------------------------------------------------------- */
void re_insert_simple(T_inst& inst, T_trip& t) {
	int detour[nmax_clients_par_tournee][3];  // [0] = client, [1] = detour, [2] = where in the tournee
	int stop = 0;
	int i    = 1;
	int j    = 1;

	remplir_detour(inst, t, detour);

	/* Tant que l'on a pas d'amelioration et qu'on a pas juge tous les clients */
	while (!stop) {
		
		j = 1;

		/* Tant que l'on a pas d'amelioration et qu'on a pas essaye tous les echanges */
		while (!stop) {
			
			/* Si on est trop loin */
			if (j > t.nb) break;

			/* Si le nouveau detour serait plus petit que le detour actuel */
			else if (j != i && detour[i][1] > inst.dist[detour[i][0]][t.liste[j - 1]] + inst.dist[detour[i][0]][t.liste[j]]) {
				decale_one(t, detour, i, j);
				stop = 1;
			}

			j++;
		}

		i++;
		if (i > t.nb) stop = 1;
	}

}

/** --------------------------------------------------------- *
  * @fn recup_tour_geant                                      *
  *                                                           *
  * @brief Permet de recuperer un tour geant a partir d'une   *
  *        liste de tournees                                  *
  *                                                           *
  * @param s {sol} structure contenant la solution et les     *
  *                tournees permettant de reconstruire le TG  *
  * --------------------------------------------------------- */
void recup_tour_geant(T_sol& sol) {
	int i = 1;

	/* Pour toutes les tournees */
	for (int tournee = 1; tournee <= sol.nb_tournees; tournee++) {

		/* Pour chaque client de la tournee */
		for (int client = 1; client <= sol.liste_t[tournee].nb; client++) {
			sol.tour_geant[i] = sol.liste_t[tournee].liste[client];
			i++;
		}

	}
}

/** --------------------------------------------------------- *
  * @fn decale                                                *
  *                                                           *
  * @brief Decale les element dans deux tournees donnees en   *
  *                parametre                                  *
  *                                                           *
  * @param t1     {T_trip}   tournee consideree               *
  * @param t2     {T_trip}   tournee consideree               *
  * @param detour {int [][]} tableau dans lequel decaler      *
  * @param i      {int}      element dans le tableau a partir *
  *                          duquel decaler                   *
  * @param j      {int}      indice de fin du decalage        *
  * --------------------------------------------------------- */
void decale(T_trip t1, T_trip t2, int detour[nmax_clients_par_tournee][3], int i, int j) {

	/* Pour la tournee 1, decalage a gauche car deletion */
	for (int a = detour[i][2]; a < t1.nb; a++) {
		t1.liste[a] = t1.liste[a + 1];
	}

	/* Pour la tournee 2, decalage a droite car insertion */
	for (int a = t2.nb + 1; a > j + 1; a--) {
		t2.liste[a] = t2.liste[a - 1];
	}
	t2.liste[j + 1] = detour[i][0];
}

/** --------------------------------------------------------- *
  * @fn re_insert_double                                      *
  *                                                           *
  * @brief Effectue le deplacement d'un client dans la tournee*
  *        consideree en reduisant le detour qu'il represente *
  *        ou dans une autre tournee donnee en parametre      *
  *                                                           *
  * @param inst {T_inst} structure contenant les infos du VRP *
  * @param t1   {T_trip} tournee consideree                   *
  * @param t2   {T_trip} autre tournee consideree             *
  * --------------------------------------------------------- */
void re_insert_double(T_inst& inst, T_trip& t1, T_trip& t2) {
	int detour[nmax_clients_par_tournee][3];  // [0] = client, [1] = detour, [2] = where in the tournee
	int stop = 0;
	int i    = 1;
	int j    = 1;
	int save;

	remplir_detour(inst, t1, detour);

	// Parcours de la tournee 1
	while (!stop) {
		j = 1;

		// On tente d'echanger dans la tournee 1
		while (!stop) {

			/* Si on est trop loin */
			if (j > t1.nb) {
				break;
			}
			
			/* Si le nouveau detour est moins grand que l'ancien */
			else if (detour[i][1] > inst.dist[detour[i][0]][t1.liste[j - 1]] + inst.dist[detour[i][0]][t1.liste[j + 1]]) {
				save = t1.liste[j];
				t1.liste[j] = detour[i][0];
				t1.liste[detour[i][2]] = save;
				stop = 1;
			}

			j++;

		}

		j = 0;

		// On tente d'inserer dans la tournee 2 (entre j et j+1)
		while (!stop) {

			// Si on a atteint la fin de la tournee 2
			if (j > t2.nb) {
				break;
			}

			// Si le detour en changeant de place est plus petit
			else if (detour[i][1] > inst.dist[detour[i][0]][t2.liste[j + 1]] + inst.dist[detour[i][0]][t2.liste[j]]) {

				//Si l'insertion respecte la capacite
				if (t2.demande[t2.nb] + inst.demande[detour[i][0]] <= inst.capacite){
					decale(t1, t2, detour, i, j);
					recalcul_demandes(inst, t1);
					recalcul_demandes(inst, t2);
					stop = 1;
				}
			}

			j++;
		}


		i++;
		/* Si on a considere tous les clients de la tournee */
		if (i > t1.nb) stop = 1;
	}

}

/** --------------------------------------------------------- *
  * @fn retourne_sequence                                     *
  *                                                           *
  * @brief Effectue le renversement d'une sequence dans la    *
  *        tournee en parametre                               *
  *                                                           *
  * @param t     {T_trip} tournee consideree                  *
  * @param start {int}    indice de depart                    *
  * @param ende  {int}    indice de fin                       *
  * --------------------------------------------------------- */
void retourne_sequence(T_trip& t, int start, int end) {
	int save;

	/* Tant qu'on a pas inverse 2 a 2 tous les elements */
	while (end > start) {
		save = t.liste[start];
		t.liste[start] = t.liste[end];
		t.liste[end] = save;

		end --;
		start ++;
	}
}

/** --------------------------------------------------------- *
  * @fn two_opt_simple                                        *
  *                                                           *
  * @brief Effectue le mouvement 2-opt sur une tournee        *
  *                                                           *
  * @param t     {T_trip} tournee consideree                  *
  * @param inst  {T_inst} Graphe considere                    *
  * --------------------------------------------------------- */
void two_opt_simple(T_inst& inst, T_trip& t) {
	int i,
		j,
		calcul = 0,
		stop   = 0;

	i = 1;

	/* Tant qu'on a pas tout essaye */
	while(!stop){
		j = i + 2;

		/* Tant qu'on a pas trouve d'amelioration */
		while (!stop) {
			calcul = inst.dist[t.liste[i]][t.liste[j - 1]] + inst.dist[t.liste[j]][t.liste[i + 1]]
				- inst.dist[t.liste[i]][t.liste[i + 1]] - inst.dist[t.liste[j]][t.liste[j - 1]];

			/* Si on a une amelioration */
			if (calcul < 0) {
				retourne_sequence(t, i + 1, j - 1);
				i = 0;
				break;
			}

			j++;

			/* Si on a atteint la fin de la tournee */
			if (j > t.nb) {
				break;
			}
		}

		i++;

		/* Si on a considere tous les sommets */
		if (i > t.nb - 2) {
			stop = 1;
		}
	}

}

/** --------------------------------------------------------- *
  * @fn echange                                               *
  *                                                           *
  * @brief Effectue un echange entre les deux tournees        *
  *                                                           *
  * @param t1 {T_trip} tournee consideree                     *
  * @param t2 {T_trip} tournee consideree                     *
  * @param i1 {int}    indice du depart de l'echange dans t1  *
  * @param i2 {int}    indice du depart de l'echange dans t2  *
  * @param j1 {int}    indice de la fin de l'echange dans t1  *
  * @param j2 {int}    indice de la fin de l'echange dans t2  *
  * --------------------------------------------------------- */
void echange(T_trip& t1, T_trip& t2, int i1, int i2, int j1, int j2) {
	int t1_p[nmax_clients_par_tournee];
	int t2_p[nmax_clients_par_tournee];
	int i, j,
		nb1,
		nb2,
		j1_p,
		j2_p;

	memset(t1_p, 0, sizeof(t1_p));
	memset(t2_p, 0, sizeof(t2_p));
	// Pour la tournee t1 :
	i = i1 + 1;

	/* Copie du morceau de t2 a echanger dans t1 */
	for (j = i2 + 1; j < j2; j++) {
		t1_p[i] = t2.liste[j];
		i++;
	}
	j1_p = i;

	/* Copie du reste de t1 */
	for (j = j1; j <= t1.nb; j++) {
		t1_p[i] = t1.liste[j];
		i++;
	}

	nb1 = i;

	// Pour la tournee t2 :
	i = i2 + 1;

	/* Copie du morceau de t1 a echanger dans t2*/
	for (j = i1 + 1; j < j1; j++) {
		t2_p[i] = t1.liste[j];
		i++;
	}
	j2_p = i;

	/* Copie du reste de t2 */
	for (j = j2; j <= t2.nb; j++) {
		t2_p[i] = t2.liste[j];
		i++;
	}
	nb2 = i;

	/* Recopie dans t1 de la bonne sequence */
	for (i = i1 + 1; i <= nb1; i++) {
		t1.liste[i] = t1_p[i];
	}

	/* Recopie dans t2 de la bonne sequence */
	for (i = i2 + 1; i <= nb2; i++) {
		t2.liste[i] = t2_p[i];
	}
	t1.nb = nb1;
	t2.nb = nb2;

	retourne_sequence(t1, i1 + 1, j1_p - 1);
	retourne_sequence(t2, i2 + 1, j2_p - 1);
}

/** --------------------------------------------------------- *
  * @fn two_opt_double                                        *
  *                                                           *
  * @brief Effectue le mouvement 2-opt sur deux tournees      *
  *                                                           *
  * @param t1    {T_trip} tournee consideree                  *
  * @param t2    {T_trip} tournee consideree                  *
  * @param inst  {T_inst} Graphe considere                    *
  * --------------------------------------------------------- */
void two_opt_double(T_inst& inst, T_trip& t1, T_trip& t2) {
	int i1,
		i2,
		j2,
		j1,
		calcul = 0,
		stop   = 0;

	/* Tant qu'on a pas considere toutes les solutions */
	while (!stop) {
		
		stop = 1;

		/* Pour tous les client de t1 */
		for (i1 = 1; i1 < t1.nb - 1; i1++) {

			/* Selection du morceau de t1 a echanger */
			for (j1 = i1 + 2; j1 <= t1.nb; j1++) {

				calcul = -inst.dist[t1.liste[i1]][t1.liste[i1 + 1]] - inst.dist[t1.liste[j1]][t1.liste[j1 - 1]];

				/* Pour tous les clients de t2 */
				for (i2 = 1; i2 < t2.nb - 1; i2++) {

					calcul -= inst.dist[t2.liste[i2]][t2.liste[i2 + 1]];
					calcul += inst.dist[t1.liste[i1]][t2.liste[i2 + 1]] + inst.dist[t1.liste[j1 - 1]][t2.liste[i2]];

					/* Selection du morceau de t2 a echanger */
					for (j2 = i2 + 2; j2 <= t2.nb; j2++) {

						calcul += inst.dist[t1.liste[j1]][t2.liste[j2 - 1]] + inst.dist[t1.liste[i1 + 1]][t2.liste[j2]] - inst.dist[t2.liste[j2]][t2.liste[j2 - 1]];
						
						/* Si on a une amelioration */
						if (calcul < 0) {

							/* Si l'echange respecte la capacite */
							if (t1.demande[t1.nb] - t1.demande[j1 - 1] + t1.demande[i1] + t2.demande[j2 - 1] - t2.demande[i2] <= inst.capacite &&
								t2.demande[t2.nb] - t2.demande[j2 - 1] + t2.demande[i2] + t1.demande[j1 - 1] - t1.demande[i1] <= inst.capacite) {
								echange(t1, t2, i1, i2, j1, j2);
								recalcul_demandes(inst, t1);
								recalcul_demandes(inst, t2);
							}
						}

					}

				}

			}

		}

		
	}
}

/** --------------------------------------------------------- *
  * @fn permuter                                              *
  *                                                           *
  * @brief Effectue le mouvement 2-opt sur deux tournees      *
  *                                                           *
  * @param choice    {int[]} Tableau dans lequel faire la     *
  *	                         permutation                      *
  * @param c         {int}   Indice ou faire la permutation   *
  * @param direction {int}   Direction de la permutation (1 = *
  *                          droite, -1 = gauche de c)        *
  * --------------------------------------------------------- */
void permuter(int choice[4], int c, int direction) {
	int save = choice[c];

	/* Choix de la direction de permutation et verification de la faisabilite */
	if ((direction == -1 && c != 0) || (direction == 1 && c != 3)) {
		choice[c] = choice[c + direction];
		choice[c + direction] = save;
	}
}


/** --------------------------------------------------------- *
  * @fn recherche_locale                                      *
  *                                                           *
  * @brief Effectue la recherche locale sur un tour geant     *
  *        donne en parametre. On a un tableau de 4 cases     *
  *        representant les quatre choix et leur proba        *
  *        d'etre choisi (70% de chance de prendre le premier *
  *        puis on refait un tirage avec 70% de chance de     *
  *        prendre le deuxieme, etc.)                         *
  *  0 = re-insert simple                                     *
  *  1 = re-insert double                                     *
  *  2 = 2-opt simple                                         *
  *  3 = 2-opt double                                         *
  *                                                           *
  * @param inst {T_inst} Structure contenant les donnes du VRP*
  * @param sol  {T_sol}  Structure sur laquelle faire la      *
  *                      recherche locale contenant le tour   *
  *                      geant d'origine                      *
  * @param nb_iter {int} Nombre max d'iterations a faire      *
  * --------------------------------------------------------- */
void recherche_locale(T_inst & inst, T_sol & sol, int nb_iter) {
	int choice[4];
	int c;
	int debut[4];
	int r;
	int t1;
	int t2;
	int cout;
	int iter = 0;

	T_sol s1;

	memset(debut, 0, sizeof(debut));

	evaluer(inst, sol);

	/* Initialisation des mouvements dans le tableau de choix */
	for (int i = 0; i < 4; i++) {
		do {
			r = rand() % 4;
		} while (debut[r] == 1);

		debut[r] = 1;
		choice[i] = r;
	}

	/* Pour toutes les iterations */
	for (int i = 0; i < nb_iter; i++) {
		r = rand() % 101;
		c = 0;
		cout = sol.cout;
		s1 = sol;

		/* Tant qu'on a pas trouve le mouvement voulu */
		while (c < 3 && r > 70) {
			r = rand() % 101;
			c++;
		}

		switch (choice[c]) {
			/* Re-insert simple */
			case 0:
				re_insert_simple(inst, sol.liste_t[rand() % sol.nb_tournees + 1]);
				break;
			
			/* Re-insert double */
			case 1:
				/* Si on a plus d'une tournee */
				if (sol.nb_tournees > 1) {
					t1 = rand() % sol.nb_tournees + 1;

					/* Recherche d'une tournee differente */
					do {
						t2 = rand() % sol.nb_tournees + 1;
					} while (t2 == t1);

					re_insert_double(inst, sol.liste_t[t1], sol.liste_t[t2]);
				}
				break;

			/* 2-opt simple */
			case 2:
				two_opt_simple(inst, sol.liste_t[rand() % sol.nb_tournees + 1]);
				break;

			/* 2-opt double */
			case 3:
				/* Si on a plus d'une tournee */
				if (sol.nb_tournees > 1) {
					t1 = rand() % sol.nb_tournees + 1;

					/* Recherche d'une tournee diffferente */
					do {
						t2 = rand() % sol.nb_tournees + 1;
					} while (t2 == t1);
					two_opt_double(inst, sol.liste_t[t1], sol.liste_t[t2]);
				}
				break;
		}

		recup_tour_geant(sol);
		evaluer(inst, sol);

		permuter(choice, c, (cout > sol.cout) ? -1 : 1);

		/* Si on a une meilleure solution */
		if (cout < sol.cout) {
			sol = s1;
		}
	}

}

/** --------------------------------------------------------- *
  * @fn hashage                                               *
  *                                                           *
  * @brief Retourne le hash de la solution donnee en parametre*
  *                                                           *
  * @param inst {T_inst} Structure contenant les donnes du VRP*
  * @param sol  {T_sol}  Structure contenant la solution a    *
  *                      hasher                               *
  * --------------------------------------------------------- */
long long int hashage(T_inst &inst, T_sol& sol) {
	long long int hash = 0;

	/* Pour tous les clients, on ajoute leur date d'arrivee */
	for (int i = 1; i <= inst.n; i++) {
		hash = (hash + sol.m[i]) % K;
	}

	return hash;
}


 /** --------------------------------------------------------- *
   * @fn permut                                                *
   *                                                           *
   * @brief Realise une permutation simple du tour geant donne *
   *        dans la structure de solution donnee en parametre  *
   *                                                           *
   * @param inst {T_inst} Structure contenant les donnes du VRP*
   * @param sol  {T_sol}  Structure contenant la solution ou   *
   *                      faire la permutation                 *
   * --------------------------------------------------------- */
void permut(T_sol& s, T_inst& inst) {
	int save;
	int rand1 = rand() % inst.n + 1;
	int rand2 = rand() % inst.n + 1;

	/* Tant que la permutation ne change pas
	   le vecteur                            */
	while (rand1 == rand2) {
		rand2 = rand() % inst.n + 1;
	}

	save = s.tour_geant[rand1];
	s.tour_geant[rand1] = s.tour_geant[rand2];
	s.tour_geant[rand2] = save;
}

/** --------------------------------------------------------- *
  * @fn grasp                                                 *
  *                                                           *
  * @brief Fonction qui parcourre l'espace pour trouver la    *
  *        solution optimale                                  *
  *                                                           *
  * @param inst {T_inst} Structure contenant les donnes du VRP*
  * @param sol  {T_sol}  Structure contenant la solution ou   *
  *                      faire la permutation                 *
  * @param nb_iter {int} Nombre max d'iterations              *
  * @param nb_els  {int} nombre max de recherche des voisins  *
  * @param nb_voisins {int} nombre de voisins a chercher      *
  * --------------------------------------------------------- */
void grasp(T_sol &s, T_inst &inst, int nb_iter, int nb_els, int nb_voisins) {
	T_sol         best_voisin;
	T_sol         max;
	T_sol         sol_els;
	int           r;
	int           nb_r;
	int           voisins;
	int           iter;
	long long int hash;

	max.cout = c_inf;
	memset(Ti, 0, sizeof(Ti));

	/* Faire le meme traitement pour le nombre d'iterations voulues */
	for (int i = 0; i < nb_iter; i++) {

		nb_r = 0;
		/* Tant que l'on a deja visite la solution consideree */
		do {

			r = rand() % 101;
			if (r < 33) {
				generer_tour_geant(inst, s);
			}
			else if (r < 66) {
				plus_proche_voisin(inst, s);
			}
			else {
				autre_heuristique(inst, s);
			}
			recherche_locale(inst, s, 10);
			hash = hashage(inst, s);

		} while (Ti[hash] == 1 && nb_r < 100);

		Ti[hash] = 1;

		/* Visiter un espace voisin */
		for (int j = 0; j < nb_els; j++) {
			best_voisin = s;
			voisins = 0;
			iter = 0;

			/* Tant que l'on a pas trouve assez de voisins ou que l'on a
			   depasse un nombre maximum d'essais et que l'on a au moins
			   un voisin                                                */
			while (voisins < nb_voisins && (iter < nb_iter || voisins == 0)) {
				sol_els = s;
				permut(sol_els, inst);
				recherche_locale(inst, sol_els, 10);
				hash = hashage(inst, sol_els);

				/* Si on n'a pas deja visite cette solution */
				if (Ti[hash] == 0) {
					Ti[hash] = 1;

					/* Si la solution est meilleure que celle retenue jusque-la */
					if (best_voisin.cout >= sol_els.cout) {
						best_voisin = sol_els;
					}

					voisins++;
				}

				iter++;
			}

			s = best_voisin;

			/* Si la solution est meilleure que toutes les autres
			   jusqu'a maintenant                                 */
			if (s.cout < max.cout) {
				max = s;
			}
		}
	}

	s = max;

}
